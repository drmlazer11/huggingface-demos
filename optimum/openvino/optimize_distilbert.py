# Load original model
# DistilBERT model fine-tuned on a subset of the Amazon US reviews dataset

from transformers import AutoTokenizer, AutoModelForSequenceClassification, pipeline

model_id = "juliensimon/distilbert-amazon-shoe-reviews"
task_type = "text-classification"

save_dir = "./quantized_model"
model = AutoModelForSequenceClassification.from_pretrained(model_id)
tokenizer = AutoTokenizer.from_pretrained(model_id)
pipe = pipeline(task_type, model=model, tokenizer=tokenizer)

# Evaluate original model

import evaluate
from datasets import load_dataset

dataset_id = "juliensimon/amazon-shoe-reviews"

data = load_dataset(dataset_id, split='test')
metric = evaluate.load("accuracy")
evaluator = evaluate.evaluator(task_type)


def evaluate_pipeline(pipeline):
    results = evaluator.compute(
        model_or_pipeline=pipeline,
        data=data,
        metric=metric,
        label_column="labels",
        label_mapping=model.config.label2id,
    )
    return results


results = evaluate_pipeline(pipe)
print(results)

# Create a quantizer object

from optimum.intel.openvino import OVConfig, OVQuantizer

quantization_config = OVConfig()
print(quantization_config)

def tokenize(batch):
    return tokenizer(batch["text"], padding="max_length", truncation=True)

quantizer = OVQuantizer.from_pretrained(model)
calibration_dataset = quantizer.get_calibration_dataset(
    dataset_id,
    num_samples=1000,
    dataset_split="train",
    preprocess_function=tokenize,
    preprocess_batch=True,
)

# Apply static quantization and export the quantized model to OpenVINO IR format

quantizer.quantize(
    quantization_config=quantization_config,
    calibration_dataset=calibration_dataset,
    save_directory=save_dir,
)

tokenizer.save_pretrained(save_dir)

# Evaluate the quantized model

from optimum.intel.openvino import OVModelForSequenceClassification
from transformers import pipeline

ov_model = OVModelForSequenceClassification.from_pretrained(save_dir)
pipe = pipeline(task_type, model=ov_model, tokenizer=tokenizer)
results = evaluate_pipeline(pipe)
print(results)
